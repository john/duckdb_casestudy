# README

<!-- badges: start -->

<!-- badges: end -->

This code demonstrates importing voluminous, larger-than-RAM, data into a DuckDB database file.  Then, query the DuckDB database file using SQL and {dplyr}.

## See Also:

- See Also: https://github.com/duckdb/duckdb_arrow

- See Also: https://people.duke.edu/~jrl/bigdata/ duckdb_parquest.zip (3.6 G)

